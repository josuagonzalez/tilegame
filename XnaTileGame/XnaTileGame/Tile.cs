﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XnaTileGame
{
    class Tile
    {
        public int Id;
        public int GridXPos;
        public int GridYPos;
        private Tileset _tileset;
        public Sprite sprite;

        public Tile(int tileId, int x, int y, Tileset theTileset, double opacity = 1.0f)
        {
            Id = tileId;
            GridXPos = x;
            GridYPos = y;
            _tileset = theTileset;
            sprite = new Sprite(_tileset.texture);
            sprite.SubRect = _tileset.GetTileRect(Id);
            sprite.Position.X = x * _tileset.spriteSize.X;
            sprite.Position.Y = y * _tileset.spriteSize.Y;
            sprite.Opacity = (float)opacity;
        }

    }
}
