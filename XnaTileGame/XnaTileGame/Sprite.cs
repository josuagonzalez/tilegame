﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace XnaTileGame
{
    class Sprite
    {
        private Texture2D _texture;
        public Vector2 Position = Vector2.Zero;
        public Rectangle Bounds = Rectangle.Empty;
        public Vector2 Origin = Vector2.Zero;
        public Rectangle SubRect { get; set; }
        public float Scale = 1.0f;
        public float RotationAngle = .0f;
        public float Opacity = 1.0f;

        public Sprite()
        {

        }

        public Sprite(Texture2D texture)
        {
            _texture = texture;
            Bounds = texture.Bounds;
            SubRect = Bounds;
        }

        public void Update(GameTime gt)
        {
        }

        public void Draw(GameTime gt, SpriteBatch sb)
        {
            sb.Draw(_texture, Position, SubRect, new Color(1.0f, 1.0f, 1.0f, Opacity),  RotationAngle, Origin, Scale, SpriteEffects.None, 0f);
        }
    }
}
