﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledSharp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace XnaTileGame
{
    class Map
    {
        private TmxMap _map;
        private int _tileWidth;
        private int _tileHeight;
        private Rectangle _mapBounds;
        private List<MapLayer> _mapLayers;
        private ContentManager _content;
        private Entity _player;
        protected KeyboardState _keyState;
        private List<GameObject> _baselineList;
        
        public Map()
        {

        }

        private void LoadMap(string filename)
        {
            var tileset = new Tileset(_content.Load<Texture2D>("spritesheet"), 1, new Vector2(64,64));
            _map = new TmxMap(filename);
            _tileWidth = _map.TileWidth;
            _tileHeight = _map.TileHeight;
            _mapBounds = new Rectangle(0, 0, _tileWidth * _map.Width, _tileHeight * _map.Height);
            foreach (TmxTileset tmxTset in _map.Tilesets)
            {
                Console.WriteLine("Tileset found : " + tmxTset.ToString());
            }


            _mapLayers = new List<MapLayer>();
            foreach (TmxLayer tmxLayer in _map.Layers)
            {
                var layer = new MapLayer() { Opacity = tmxLayer.Opacity };

                //int y = 0;
                foreach (TmxLayerTile tile in tmxLayer.Tiles)
                {
                    if(tile.Gid > 0)
                        layer.Tiles.Add(new Tile(tile.Gid, tile.X, tile.Y, tileset, layer.Opacity));
                }

                try
                {
                    if (tmxLayer.Properties["isMainLayer"].Equals("true")) layer.IsMainLayer = true;
                }
                catch (KeyNotFoundException) {}


                _mapLayers.Add(layer);
            }

            foreach (TmxObjectGroup og in _map.ObjectGroups)
            {
                var layer = new MapLayer();
                foreach (TmxObjectGroup.TmxObject obj in og.Objects)
                {
                    layer.Objects.Add(new GameObject() { Rect = new Rectangle(obj.X, obj.Y, obj.Width, obj.Height) });
                }
                _mapLayers.Add(layer);
            }
        }

        public void InitMap(string filename, ContentManager cm)
        {
            _content = cm;
            LoadMap(filename);
            _player = new Entity(cm.Load<Texture2D>("mario"));
            _player.Scale = 2.0f; 
        }

        public void Update(GameTime gt)
        {
            _keyState = Keyboard.GetState();
            //Check Move
            if (_keyState.IsKeyDown(Keys.Right))
            {
                _player.Position.X += 2.5f;
            }
            if (_keyState.IsKeyDown(Keys.Left))
            {
                _player.Position.X -= 2.5f;
            }
            if (_keyState.IsKeyDown(Keys.Down))
            {
                _player.Position.Y += 2.5f;
            }
            if (_keyState.IsKeyDown(Keys.Up))
            {
                _player.Position.Y -= 2.5f;
            }


            _player.Position.Y += 2.5f;
            _player.Update(gt);

            // Entity collision

            // entity update
            foreach (MapLayer layer in _mapLayers)
            {
                foreach (Object entity in layer.Entities)
                {

                }
            }

        }

        public void Draw(GameTime gt, SpriteBatch sb)
        {
            bool isPlayerDrawn = false;
            foreach (MapLayer layer in _mapLayers)
            {
                foreach (Tile tile in layer.Tiles)
                {
                    tile.sprite.Draw(gt, sb);
                }

                foreach (GameObject obj in layer.Objects)
                {
                    Texture2D SimpleTexture = new Texture2D(sb.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
                    sb.Draw(SimpleTexture, new Rectangle(obj.Rect.Left, obj.Rect.Top, obj.Rect.Width, 1), Color.Red);
                }

                if (layer.IsMainLayer && !isPlayerDrawn)
                {
                    _player.Draw(gt, sb);
                    isPlayerDrawn = true;
                }
            }

            if (!isPlayerDrawn) _player.Draw(gt, sb);
        }
    }
}
