﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XnaTileGame
{
    class MapLayer
    {
        public List<GameObject> Objects = new List<GameObject>();
        public List<Tile> Tiles = new List<Tile>();
        public List<Object> Entities = new List<Object>();

        public bool IsMainLayer = false;
        public double Opacity = 1.0f;
    }
}
