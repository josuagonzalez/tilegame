﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace XnaTileGame
{
    class Tileset
    {
        public Texture2D texture;
        public string name;
        public int FirstId;
        public Vector2 spriteSize = Vector2.Zero;

        public Tileset(Texture2D texture, int firstId, Vector2 spriteSize) {
            this.texture = texture;
            this.FirstId = firstId;
            this.spriteSize = spriteSize;
        }

        public Rectangle GetTileRect(int id) {
            int tileCountX = texture.Bounds.Width / (int)spriteSize.X;
            int y = (id - FirstId) / ((tileCountX > 0) ? tileCountX : 1);
            
            return new Rectangle(((id - FirstId) - (y * tileCountX)) * (int)spriteSize.X, y * (int)spriteSize.X, (int)spriteSize.X, (int)spriteSize.Y);
        }
    }
}
