﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace XnaTileGame
{
    class GameObject
    {
        public Rectangle Rect = Rectangle.Empty;
        public Vector2 Position = Vector2.Zero;
        bool Visible = false;
        double Rotation = 0.0d;
    }
}
